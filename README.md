To run the project, follow the below steps:-

1. Clone the project using the link https://gitlab.com/prakshep_afrin/GithubSearch.git by command git clone or download the zip folder.
2. If the angular/cli not installed, write command in terminal ,sudo npm install -g @angular/cli@next.
3. To run the project, write the command ng serve.
4. Make sure you have proper versions of Node and npm installed before installing angular/cli.
5. In order to update the angular version ,update the angular by ng update@angular/cli .
5. The project will run at 'http://localhost:4200/ 'after ng-serve command.